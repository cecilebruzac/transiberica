<?php

function connection_tpl_function($atts)
{
    extract(shortcode_atts(array(
        'departure' => '',
        'arrival' => false
    ), $atts));


    $content = get_theme_mod('connection_tpl');

    if ($content) {
        $content = str_replace(
            array('[departure]', '[arrival]'),
            array($departure, $arrival),
            $content
        );

        $shortcoded_content = do_shortcode($content);

        return $shortcoded_content;
    }

    return '';
}
add_shortcode('connection_tpl', 'connection_tpl_function');
