<?php

function header_function($atts, $content = null)
{
    extract(shortcode_atts(array(
        'cover' => '',
        'covers' => '',
        'colored' => '',
        'smaller' => '',
    ), $atts));

    $extra_classes = ($colored == true ? 'bg-dark-primary' : 'bg-alt-black') . ' text-white [&_a:not(.cta)]:text-white';

    $bg = '<div class="absolute top-0 left-0 w-full h-full flex ' . ($colored == true ? 'opacity-40' : 'opacity-80') . ' ">';

    if ($covers) {
        $covers_array = explode(',', $covers);

        if ($covers_array >= 2) {
            $left_image_id = attachment_url_to_postid($covers_array[0]);
            $right_image_id = attachment_url_to_postid($covers_array[1]);

            $img_classes = "!h-full !max-w-none object-cover object-center w-1/2";

            if ($left_image_id) {
                $bg .= wp_get_attachment_image($left_image_id, array('700', '99999'), "", array("class" => $img_classes, "sizes" => "50vw"));
            } else {
                $bg .= '<img alt="" class="' . $img_classes . '" src="' . $covers_array[0] . '" />';
            }
            if ($right_image_id) {
                $bg .= wp_get_attachment_image($right_image_id, array('700', '99999'), "", array("class" => $img_classes, "sizes" => "50vw"));
            } else {
                $bg .= '<img alt="" class="' . $img_classes . '" src="' . $covers_array[1] . '" />';
            }
        }
    } else if ($cover) {
        $image_id = attachment_url_to_postid($cover);

        $img_classes = "w-full !h-full !max-w-none object-cover object-center";

        if ($image_id) {
            $bg .= wp_get_attachment_image($image_id, array('1400', '99999'), "", array("class" => $img_classes, "sizes" => "100vw"));
        } else {
            $bg .= '<img alt="" class="' . $img_classes . '" src="' . $cover . '" />';
        }
    }

    $bg .= '</div>';

    $extra_classes .= $smaller == true ? ' min-h-[15.5rem] lg:min-h-[31rem]' : ' min-h-[25.5rem] lg:min-h-[31rem]';

    $shortcoded_content = do_shortcode($content);

    $output = '<header class="relative flex flex-col justify-center [&_h1]:text-shadow [&_h1]:big-title [&_h2]:big-title ' . $extra_classes . ' ">';
    $output .= $bg;
    $output .= '<div class="relative z-[1] centered-row leading-0 py-6 text-center">' . $shortcoded_content . '</div>';
    $output .= '</header>';

    return $output;
}
add_shortcode('header', 'header_function');

function hero_function($atts, $content = null)
{
    extract(shortcode_atts(array(
        'cover' => '',
    ), $atts));

    $shortcoded_content = do_shortcode($content);

    $topbar_height = (141 / 16) . 'rem';

    $image_id = attachment_url_to_postid($cover);

    $bg = '<div class="absolute top-0 left-0 w-full h-full">';
    $img_classes = "w-full !h-full !max-w-none object-cover object-center";

    if ($image_id) {
        $bg .= wp_get_attachment_image($image_id, array('1400', '99999'), "", array("class" => $img_classes, "sizes" => "100vw"));
    } else {
        $bg .= '<img alt="" class="' . $img_classes . '" src="' . $cover . '" />';
    }

    $bg .= '</div>';

    /* TODO: rwd cover image */
    $output = '<section style="--topbar-height:' . $topbar_height . '" class="bg-grey [&_a:not(.cta)]:text-white text-white overflow-hidden relative">';
    $output .= $bg;
    $output .= '<div class="centered-row relative z-[1]">';
    $output .= '<div class="leading-0 py-6 lg:ml-auto lg:w-1/2 flex flex-col justify-center max-lg:text-center min-h-[25.5rem] lg:min-h-[calc(100vh-var(--topbar-height))] [&_h1]:big-title [&_h2]:big-title">' . $shortcoded_content . '</div>';
    $output .= '</div>';
    $output .= '</section>';

    return $output;
}
add_shortcode('hero', 'hero_function');
