<?php
function cta_function($atts, $content = null)
{
    extract(shortcode_atts(array(
        'href' => '',
        'bigger' => false,
        'negative' => false
    ), $atts));

    $output = '<a class="cta' . ($bigger ? ' cta--bigger' : '') . ($negative ? ' cta--negative' : '') . '" href="' . $href . '">' . $content . '</a>';
    return $output;
}

add_shortcode('cta', 'cta_function');

function ctas_row_function($atts, $content = null)
{
    $shortcoded_content = do_shortcode($content);
    $output = '<span class="ctas block lg:flex max-lg:[&_.cta]:my-4 max-lg:[&_.cta:first-child]:mt-0 max-lg:[&_.cta:last-child]:mb-0 max-lg:[&_.cta]:mx-auto max-lg:[&_.cta]:flex max-lg:[&_.cta]:w-fit lg:-mx-4 lg:[&_.cta]:mx-4 lg:justify-center lg:items-baseline">' . $shortcoded_content . '</span>';
    return $output;
}
add_shortcode('ctas_row', 'ctas_row_function');

function ctas_function($atts, $content = null)
{
    extract(shortcode_atts(array(
        'center' => ''
    ), $atts));
    $shortcoded_content = do_shortcode($content);
    $output = '<span class="ctas ' . ($center ? 'lg:text-center' : '') . ' w-full block -m-2 [&_.cta]:m-2 md:-m-3 [&_.cta]:md:m-3">' . $shortcoded_content . '</span>';
    return $output;
}
add_shortcode('ctas', 'ctas_function');

function ctas_section_function($atts, $content = null)
{
    $content = get_theme_mod('ctas');
    if ($content) {
        $shortcoded_content = do_shortcode($content);
        $sm_cover = get_stylesheet_directory_uri() . '/assets/images/ctas-section-sm-bg.webp';
        $cover = get_stylesheet_directory_uri() . '/assets/images/ctas-section-bg.webp';


        $output = '<section class="ctas-section bg-dark-primary text-white [&_a:not(.cta)]:text-white not-last:mb-8 lg:not-last:not-last:mb-16 relative">';
        $output .= '<img class="w-full !h-full !max-w-none object-cover object-center absolute top-0 left-0" alt="" src="' . $cover . '" srcset="' . $cover  . ' 1400w,' . $sm_cover  . ' 700w" sizes="100vw"/>';
        $output .= '<div class="relative centered-row centered-row--smaller leading-0 py-16 text-center [&_p]:slide-text [&_p:not(:first-child)]:mt-11 [&_.ctas]:mt-4">' . $shortcoded_content . '</div>';
        $output .= '</section>';

        return $output;
    }
}
add_shortcode('ctas_section', 'ctas_section_function');
