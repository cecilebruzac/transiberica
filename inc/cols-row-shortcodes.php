<?php

function cols_row_function($atts, $content = null)
{
    $shortcoded_content = do_shortcode($content);
    $output = '<div class="lg:flex lg:space-x-12 centered-row mt-8 lg:mt-16">' . $shortcoded_content . '</div>';
    return $output;
}
add_shortcode('cols_row', 'cols_row_function');

function col_function($atts, $content = null)
{
    $shortcoded_content = do_shortcode($content);
    $output = '<div class="lg:[&_>_p]:text-justify lg:flex-1 content max-lg:not-first:mt-8">' . $shortcoded_content . '</div>';
    return $output;
}
add_shortcode('col', 'col_function');
