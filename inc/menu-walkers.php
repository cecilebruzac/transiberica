<?php

class Features_Menu_Walker extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $output .= '<li role="none">';

        $classes = (is_array($item->classes) && count($item->classes) > 0 ? implode(" ", $item->classes) : '');

        if ($item->url && $item->url != '#') {
            $output .= '<a role="menuitem" class="' .  $classes . '" href="' . $item->url . '">';
        } else {
            $output .= '<span class="' . $classes . '">';
        }

        $output .= $item->title;

        if ($item->url && $item->url != '#') {
            $output .= '</a>';
        } else {
            $output .= '</span>';
        }
    }
}

class Footer_Menu_Walker extends Walker_Nav_Menu
{
    private $curItem;

    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $this->curItem = $item;

        $classes = (is_array($item->classes) && count($item->classes) > 0 ? implode(" ", $item->classes) : '');
        $output .= '<li role="none" class="' .  $classes . ($depth === 0 ? ' title mt-6 lg:mt-8' : ' basic-text') . ' [&_ul]:max-lg:hidden">';

        if ($item->url && $item->url != '#') {
            $output .= '<a role="menuitem" class="[&+ul]:lg:mt-6" href="' . $item->url . '">';
        } else {
            $output .= '<span class="[&+ul]:lg:mt-6">';
        }

        $output .= $item->title;

        if ($item->url && $item->url != '#') {
            $output .= '</a>';
        } else {
            $output .= '</span>';
        }
    }

    public function start_lvl(&$output, $depth = 0, $args = null)
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat($t, $depth);

        $classes = array('sub-menu');

        $submenu_classes = apply_filters('nav_menu_submenu_css_class', $classes, $args, $depth);

        $class_names = (is_array($submenu_classes) && count($submenu_classes) > 0 ? implode(" ", $submenu_classes) : '');

        $atts = array();
        $atts['class'] = !empty($class_names) ? $class_names : '';
        $atts['aria-label'] = 'Sous-menu ' . $this->curItem->title;

        $attributes = $this->build_atts($atts);

        $output .= "{$n}{$indent}<ul role=\"menu\" {$attributes}>{$n}";
    }
}

class Main_Nav_Menu_Walker extends Walker_Nav_Menu
{
    private $curItem;

    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $this->curItem = $item;

        $has_children = !empty($item->classes) && is_array($item->classes) && in_array('menu-item-has-children', $item->classes);

        $li_extra_classes = ' lg:relative group max-lg:text-center max-lg:not-first:mt-8';

        if ($depth === 1) {
            $li_extra_classes .= ' not-first:lg:border-t border-light-grey lg:bg-white';
        }

        $classes = (is_array($item->classes) && count($item->classes) > 0 ? implode(" ", $item->classes) : '');

        $output .= '<li role="none" class="' . $classes . $li_extra_classes . '">';

        $extra_classes = 'lg:py-3 max-lg:title lg:uppercase lg:text-sm lg:block';
        if ($item->url && $item->url != '#') {
            if ($depth === 1) {
                $extra_classes .= ' lg:bg-white lg:px-4';
            }
            $extra_classes .= (!!$item->current || !!$item->current_item_ancestor) ? '' : ' lg:text-grey lg:hocus:text-black transition-colors';
            $output .= '<a tabindex="0" ' . ($has_children ? 'aria-haspopup="true"' : '') . ' role="menuitem" class="' . $extra_classes . '" href="' . $item->url . '">';
        } else {
            $output .= '<span class="' . $extra_classes . '">';
        }

        $output .= $item->title;
        $output .= ($item->url && $item->url != '#') ? '</a>' : '</span>';
    }

    public function start_lvl(&$output, $depth = 0, $args = null)
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat($t, $depth);

        $classes = array('sub-menu', 'max-lg:hidden z-10 sr-only !absolute top-full shadow-nav min-w-[12.5rem] group-has-[a:focus-visible]:lg:not-sr-only group-hover:lg:not-sr-only');

        $submenu_classes = apply_filters('nav_menu_submenu_css_class', $classes, $args, $depth);

        $class_names = (is_array($submenu_classes) && count($submenu_classes) > 0 ? implode(" ", $submenu_classes) : '');

        $atts = array();
        $atts['class'] = !empty($class_names) ? $class_names : '';
        $atts['aria-label'] = 'Sous-menu ' . $this->curItem->title;

        $atts = apply_filters('nav_menu_submenu_attributes', $atts, $args, $depth);
        $attributes = $this->build_atts($atts);

        $output .= "{$n}{$indent}<ul role=\"menu\" {$attributes}>{$n}";
    }
}
