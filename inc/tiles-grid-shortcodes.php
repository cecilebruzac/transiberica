<?php

function tiles_grid_function($atts, $content = null)
{
    $shortcoded_content = do_shortcode($content);
    $output = '<section class="grid grid-cols-2 gap-2 md:gap-6 centered-row">' . $shortcoded_content . '</section>';
    return $output;
}
add_shortcode('tiles_grid', 'tiles_grid_function');


function tile_text_function($atts, $content = null)
{
    extract(shortcode_atts(array(
        'bg' => 'var(--off-white)',
        'text' => 'var(--black)',
    ), $atts));

    $vars = $bg != '' ? '--bg-color:' . $bg . ';' : '';
    $vars .= $text != '' ? '--text-color:' . $text . ';' : '';

    /* TODO: fit text svg */
    $output = '<div style="' . $vars . '" class="overflow-hidden bg-[var(--bg-color)] text-[var(--text-color)] h-0 p-[50%] relative w-1/2 [&_p]:key-point-text">';
    $output .= '<p class="absolute max-h-full top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-full px-4 md:px-12 lg:px-24  box-border z-[1] text-center">' . $content . '</p>';
    $output .= '</div>';

    return $output;
}
add_shortcode('tile_text', 'tile_text_function');

function tile_image_function($atts)
{
    extract(shortcode_atts(array(
        'src' => '',
    ), $atts));

    $image_id = attachment_url_to_postid($src);
    $img_classes = "absolute object-cover h-full top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-full box-border z-[1]";

    $output = '<div class="overflow-hidden h-0 p-[50%] relative w-1/2">';
    if ($image_id) {
        $output .= wp_get_attachment_image($image_id, array('528', '528'), "", array("class" => $img_classes, "sizes" => "(min-width: 1112px) 528px, (min-width: 1024px) calc(50vw-1.75rem), calc(50vw-1.25rem)"));
    } else {
        $output .= '<img alt="" class="' . $img_classes . '" src="' . $src . '" />';
    }
    $output .= '</div>';


    return $output;
}
add_shortcode('tile_image', 'tile_image_function');
