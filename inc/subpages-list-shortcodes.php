<?php

function subpages_list_function($atts)
{
    extract(shortcode_atts(array(
        'br' => '',
    ), $atts));

    $br_array_int = array();
    $br_array = explode(',', $br);

    foreach ($br_array as $each_number) {
        $br_array_int[] = (int) $each_number;
    }

    $current_page_id = get_the_ID();

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $current_page_id,
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'depth' => 1
    );

    $parent = new WP_Query($args);

    $i = 0;

    $output = '';

    if ($parent->have_posts()) {
        $output = '<div class="subpages mt-8 mb-8 lg:mb-16"><ul class="-m-1 lg:-m-3 flex flex-wrap">';

        while ($parent->have_posts()) {
            $parent->the_post();
            $id = get_the_ID();
            $link = get_the_permalink();
            $title = get_the_title();
            $thumbnail_id = get_post_meta($id, '_thumbnail_id', true);

            $output .= '<li class="page_item page_item-' . $id . ' w-1/2 lg:w-1/3 p-1 lg:p-3">';
            $output .= '<a href="' . $link . '" class="h-full flex flex-col bg-off-white !no-underline !text-black">';
            if ($thumbnail_id) $output .=  wp_get_attachment_image($thumbnail_id, array('344', '192'), "", array("class" => "w-full object-cover", "sizes" => "(min-width: 1112px) 344px, (min-width: 1024px) calc(33vw-1.6666rem), calc(50vw-1.25rem)"));
            $output .= '<div class="flex flex-1 flex-col px-4 pb-8 pt-6 md:pt-10 md:pb-16 text-center">';
            $output .= '<h3 class="!key-point-text max-w-64 mx-auto flex-1">' . $title . '</h3>';
            $output .= '<div class="cta w-min mx-auto max-md:small-text max-md:min-h-8 max-md:px-4 mt-4 md:mt-6">Nos&nbsp;liaisons</div>';
            $output .= '</div></a></li>';

            $i++;

            if (in_array($i, $br_array_int)) {
                $output .= '</ul><ul class="-mx-1 -mb-1 mt-1 lg:-mx-3 lg:-mb-3 lg:mt-3 flex flex-wrap">';
            }
        }

        $output .= '</ul></div>';
    }

    return $output;
}
add_shortcode('subpages_list', 'subpages_list_function');


function subpages_buttons_function($atts)
{

    $current_page_id = get_the_ID();

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $current_page_id,
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'depth' => 1
    );

    $parent = new WP_Query($args);

    $output = '';

    if ($parent->have_posts()) {
        $output = '[cols_row][ctas center="true"]';

        while ($parent->have_posts()) {
            $parent->the_post();
            $link = get_the_permalink();
            $title = get_the_title();
            $output .= '[cta href="' . $link . '"]' . $title . '[/cta] ';
        }

        $output .= '[/ctas][/cols_row]';
    }

    return do_shortcode($output);
}
add_shortcode('subpages_buttons', 'subpages_buttons_function');
