<?php

function text_image_section_function($atts, $content = null)
{
    extract(shortcode_atts(array(
        'image' => '',
        'squared' => false
    ), $atts));

    $shortcoded_content = do_shortcode($content);

    $wrapper_extra_classes = '';
    $sub_wrapper_extra_classes = '';

    if ($squared) {
        $wrapper_extra_classes = 'md:overflow-hidden md:h-0 md:p-[25%] md:w-1/2 md:relative';
        $sub_wrapper_extra_classes = 'md:absolute md:max-h-full md:top-1/2 md:left-1/2 md:-translate-x-1/2 md:-translate-y-1/2 w-full md:box-border md:z-[1]';
    }

    $image_id = attachment_url_to_postid($image);
    $image_classes = "md:object-cover md:h-full md:w-full md:absolute md:top-1/2 md:-translate-y-1/2";

    $output = '<article class="mt-8 lg:mt-16"><div class="md:relative">';
    $output .= '<div class="bg-off-white ' . $wrapper_extra_classes . '">';
    $output .= '<div class="p-8 lg:p-16 content ' . $sub_wrapper_extra_classes . '">' .  $shortcoded_content . '</div>';
    $output .= '</div>';
    $output .= '<div class="md:absolute md:top-0 md:right-0 md:h-full md:w-1/2 overflow-hidden">';
    if ($image_id) {
        $output .= wp_get_attachment_image($image_id, array('540', '99999'), "", array("class" => $image_classes, "sizes" => "(min-width: 1112px) 540px, (min-width: 1024px) calc(50vw-1rem), calc(100vw-2rem)"));
    } else {
        $output .= '<img alt="" class="' . $image_classes . '" src="' . $image . '" />';
    }
    $output .= '</div>';
    $output .= '</div></article>';

    return $output;
}
add_shortcode('text_image_section', 'text_image_section_function');
