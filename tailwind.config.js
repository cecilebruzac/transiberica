/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin');

module.exports = {
  jit: true,
  content: ['./**/*.php'],
  theme: {
    screen: {
      lg: {
        max: '1023px',
        min: '1024px',
      },
    },
    fontFamily: {
      sans: ['Inter', 'Arial', 'sans-serif'],
      serif: ['Newsreader', 'serif'],
    },
    extend: {
      spacing: {
        13: (13 * 4) / 16 + 'rem',
        22.5: 90 / 16 + 'rem',
        26: (80 + 24) / 16 + 'rem',
      },
      colors: {
        black: '#000000',
        'alt-black': '#1a1a1a',
        'dark-primary': '#016f44',
        primary: '#02ab69',
        grey: '#818181',
        'light-grey': '#ececec',
        'off-white': '#f5f5f5',
        white: '#ffffff',
      },
      boxShadow: {
        nav: '0px 0px 4px 0px rgba(0,0,0,0.05)',
      },
      textShadow: {
        DEFAULT: '0 0 5px var(--tw-shadow-color)',
      },
      lineHeight: {
        0: '0',
      },
      minHeight: {
        18: 72 / 16 + 'rem',
      },
      maxWidth: {
        'half-row': (540 + 32) / 16 + 'rem',
        row: (1080 + 32) / 16 + 'rem',
      },
      borderRadius: {
        field: '0.625rem',
      },
      keyframes: {
        fadeIn: {
          '0%': { opacity: 0 },
          '100%': { opacity: 1 },
        },
        fadeOut: {
          '0%': { opacity: 1 },
          '100%': { opacity: 0 },
        },
      },
      animation: {
        fadeIn: 'fadeIn .2s linear forwards',
        fadeOut: 'fadeOut .2s linear forwards',
      },
    },
  },
  plugins: [
    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          'text-shadow': (value) => ({
            textShadow: value,
          }),
        },
        { values: theme('textShadow') },
      );
    }),
    plugin(function ({ addVariant }) {
      addVariant('hocus', ['&:hover', '&:active', '&:focus']),
        addVariant('not-last', '&:not(:last-child)'),
        addVariant('not-first', '&:not(:first-child)');
    }),
    plugin(function ({ addUtilities, theme }) {
      addUtilities({
        '.layer': {
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: '0px',
          left: '0px',
        },
        '.big-title': generateTextStyle('title', {
          base: {
            fontSize: 24 / 16 + 'rem',
            fontWeight: 600,
            lineHeight: 29 / 24,
          },
          lg: {
            fontSize: 40 / 16 + 'rem',
            lineHeight: 48 / 40,
          },
        }),
        '.title': generateTextStyle('title', {
          base: {
            fontSize: 18 / 16 + 'rem',
            fontWeight: 600,
            lineHeight: 22 / 18,
          },
          lg: {
            fontSize: 25 / 16 + 'rem',
            lineHeight: 30 / 25,
          },
        }),
        '.basic-text': generateTextStyle('basic-text', {
          base: {
            fontFamily: theme('fontFamily.serif'),
            fontSize: theme('fontSize.lg'),
            lineHeight: theme('fontSize.lg[1].lineHeight'),
            fontWeight: 400,
          },
          lg: {
            fontSize: 21 / 16 + 'rem',
            lineHeight: 34 / 21,
          },
        }),
        '.small-text': generateTextStyle('small-text', {
          base: {
            fontSize: 11 / 16 + 'rem',
            lineHeight: 13 / 11,
            fontWeight: 400,
          },
          lg: {
            fontSize: 13 / 16 + 'rem',
            lineHeight: 16 / 13,
          },
        }),
        '.slide-text': generateTextStyle('slide-text', {
          base: {
            fontFamily: theme('fontFamily.serif'),
            fontSize: 26 / 16 + 'rem',
            lineHeight: 32 / 26,
            fontWeight: 400,
          },
          lg: {
            fontSize: 32 / 16 + 'rem',
            lineHeight: 42 / 32,
          },
        }),
        '.key-point-text': generateTextStyle('key-point-text', {
          base: {
            fontFamily: theme('fontFamily.sans'),
            fontSize: 10 / 16 + 'rem',
            lineHeight: 15 / 10,
            fontWeight: 500,
          },
          md: {
            fontSize: 26 / 16 + 'rem',
            lineHeight: 35 / 26,
          },
        }),
        '.form-text': generateTextStyle('form-text', {
          base: {
            fontFamily: theme('fontFamily.sans'),
            fontSize: theme('fontSize.base'),
            lineHeight: theme('fontSize.base[1].lineHeight'),
            fontWeight: 400,
          },
          md: {
            fontSize: 22 / 16 + 'rem',
            lineHeight: 27 / 22,
          },
        }),
      });
    }),
  ],
};

let camelToKebab = (s) => s.replace(/[A-Z]/g, '-$&').toLowerCase();

const generateTextStyle = (name, values) => {
  var styles = {};

  Object.entries(values.base).forEach(([key, value]) => {
    styles[key] = `var(--${name}-${camelToKebab(key)}, ${value})`;
  });

  var smStyle = {};
  if (values.sm)
    Object.entries(values.sm).forEach(([key, value]) => {
      smStyle[key] = `var(--sm-${name}-${camelToKebab(key)}, ${value})`;
    });

  var mdStyle = {};
  if (values.md)
    Object.entries(values.md).forEach(([key, value]) => {
      mdStyle[key] = `var(--md-${name}-${camelToKebab(key)}, ${value})`;
    });

  var lgStyle = {};
  if (values.lg)
    Object.entries(values.lg).forEach(([key, value]) => {
      lgStyle[key] = `var(--lg-${name}-${camelToKebab(key)}, ${value})`;
    });

  styles['@media screen(sm)'] = smStyle;
  styles['@media screen(md)'] = mdStyle;
  styles['@media screen(lg)'] = lgStyle;

  return styles;
};
