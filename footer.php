<footer class="bg-alt-black text-white max-lg:text-center">
    <div class="centered-row pt-12 pb-20">
        <div class="lg:flex">
            <article class="lg:w-1/2 lg:pr-4">
                <?php
                $contact_infos = get_theme_mod('contact_infos');
                if ($contact_infos) echo '<div class="[&_p]:basic-text [&_h2]:title [&_p:not(:first-child)]:mt-3 [&_p:not(:first-child)]:lg:mt-6 [&_h2]:mt-8">' . $contact_infos . '</div>';
                ?>
            </article>
            <?php
            wp_nav_menu(array(
                'theme_location' => 'footer',
                'container' => false,
                'container_id' => false,
                'menu_class' => 'max-lg:mt-2 [&>li]:inline-block [&>li]:w-full lg:w-1/2 lg:columns-2 [&>li]:lg:break-inside-avoid-column',
                'menu_id'  => false,
                'depth' => 0,
                'walker' => new Footer_Menu_Walker(),
                'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu" >%3$s</ul>'
            )); ?>
        </div>

        <div class="small-text text-grey lg:flex lg:justify-between mt-8 lg:mt-16">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'legals',
                'container' => false,
                'container_id' => false,
                'menu_class' => 'inline-flex space-x-2 [&_a:hover]:text-white [&_a:active]:text-white [&_a:focus]:text-white [&_a]:transition-colors [&_li+li:before]:mr-2 [&_li+li:before]:content-["-"]',
                'menu_id' => false
            ));
            ?>
            <p class="mt-2">Transiberica © 1989-<?php echo date("Y"); ?></p>
        </div>
    </div>
</footer>


<?php wp_footer(); ?>
</body>

</html>