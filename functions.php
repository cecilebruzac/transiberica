<?php

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

//setup
if (!function_exists('transiberica_setup')) {
    function transiberica_setup()
    {
        add_theme_support('custom-logo');
        $defaults = array();
        add_theme_support('logo', $defaults);
        add_theme_support('html5', ['script', 'style']);
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');

        add_image_size('thumbnail', 344, 192, true);
        add_image_size('thumbnail-hd', 688, 384, true);
        add_image_size('tile', 528, 528, true);
        add_image_size('tile-hd', 1056, 1056, true);
        add_image_size('half-content', 540, 99999);
        add_image_size('content', 1080, 99999);
        add_image_size('content-hd', 2160, 99999);
        add_image_size('banner-sm', 700, 99999);
        add_image_size('banner', 1400, 99999);
        add_image_size('banner-hd', 2800, 99999);
        add_image_size('half-banner-sm', 350, 99999);
        add_image_size('half-banner', 700, 99999);
        add_image_size('half-banner-hd', 1400, 99999);
    }
}
add_action('after_setup_theme', 'transiberica_setup', 0);

add_filter('big_image_size_threshold', '__return_false');

if (!function_exists('transiberica_scripts')) {
    function transiberica_scripts()
    {
        wp_dequeue_style('wp-block-library');
        wp_enqueue_style('transiberica-style', get_stylesheet_uri(), array(), _S_VERSION);
        wp_style_add_data('transiberica-style', 'rtl', 'replace');
        wp_register_script('script', get_template_directory_uri() . '/assets/scripts/main.min.js',  array(), _S_VERSION, true);
        wp_enqueue_script('script');
    }
}
add_action('wp_enqueue_scripts', 'transiberica_scripts');

remove_action('wp_enqueue_scripts', 'wp_enqueue_global_styles');
remove_action('wp_body_open', 'wp_global_styles_render_svg_filters');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_enqueue_scripts', 'wp_enqueue_classic_theme_styles');

if (!function_exists('transiberica_register_menus')) {
    function transiberica_register_menus()
    {
        register_nav_menus(
            array(
                'main-nav' => 'Navigation principale',
                'features' => 'Fonctionnalités',
                'footer' => 'Pied de page',
                'legals' => 'Légal',
            )
        );
    }
}
add_action('init', 'transiberica_register_menus');

require get_template_directory() . '/inc/menu-walkers.php';
require get_template_directory() . '/inc/header-shortcodes.php';
require get_template_directory() . '/inc/cta-shortcodes.php';
require get_template_directory() . '/inc/tiles-grid-shortcodes.php';
require get_template_directory() . '/inc/cols-row-shortcodes.php';
require get_template_directory() . '/inc/subpages-list-shortcodes.php';
require get_template_directory() . '/inc/text-image-shortcodes.php';
require get_template_directory() . '/inc/connection-tpl-shortcodes.php';

if (!function_exists('transiberica_customizer')) {
    function transiberica_customizer($wp_customize)
    {
        $wp_customize->add_section('banner_section', array(
            'title' => 'Bannière'
        ));

        $wp_customize->add_setting('banner_body', array(
            'default' => '',
        ));

        $wp_customize->add_control('banner_body', array(
            'label' => 'Contenu',
            'section' => 'banner_section',
            'type' => 'textarea',
        ));

        $wp_customize->add_section('contact_infos_section', array(
            'title' => 'Informations de contact'
        ));

        $wp_customize->add_setting('contact_infos', array(
            'default' => '',
        ));

        $wp_customize->add_control('contact_infos', array(
            'label' => 'Contenu',
            'section' => 'contact_infos_section',
            'type' => 'textarea',
        ));

        $wp_customize->add_section('ctas_section', array(
            'title' => 'Section des Calls To Action'
        ));

        $wp_customize->add_setting('ctas', array(
            'default' => '',
        ));

        $wp_customize->add_control('ctas', array(
            'label' => 'CTA',
            'section' => 'ctas_section',
            'type' => 'textarea',
        ));


        $wp_customize->add_section('connection_tpl_section', array(
            'title' => 'Template de la sous page Liaison'
        ));

        $wp_customize->add_setting('connection_tpl', array(
            'default' => '',
        ));

        $wp_customize->add_control('connection_tpl', array(
            'label' => 'Liaison template',
            'section' => 'connection_tpl_section',
            'type' => 'textarea',
        ));
    }
}
add_action('customize_register', 'transiberica_customizer');

if (!function_exists('wps_deregister_styles')) {
    function wps_deregister_styles()
    {
        wp_deregister_style('contact-form-7');
    }
}
add_filter('wpcf7_autop_or_not', '__return_false');
add_action('wp_print_styles', 'wps_deregister_styles', 100);

if (!function_exists('remove_empty_p')) {
    function remove_empty_p($content)
    {
        $content = force_balance_tags($content);
        $content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
        $content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content);
        return $content;
    }
}
add_filter('the_content', 'remove_empty_p', 20, 1);


//desactivate comments
if (!function_exists('foo_remove_admin_menus')) {
    function foo_remove_admin_menus()
    {
        remove_menu_page('edit-comments.php');
    }
}
add_action('admin_menu', 'foo_remove_admin_menus');

if (!function_exists('remove_comment_support')) {
    function remove_comment_support()
    {
        remove_post_type_support('post', 'comments');
        remove_post_type_support('page', 'comments');
    }
}
add_action('init', 'remove_comment_support', 100);

if (!function_exists('foo_admin_bar_render')) {
    function foo_admin_bar_render()
    {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('comments');
    }
}
add_action('wp_before_admin_bar_render', 'foo_admin_bar_render');

if (!function_exists('shortcodes_infos_meta_box')) {
    function shortcodes_infos_meta_box()
    {
        echo '<p><u>Les plus courants :</u></p>';
        echo '<ul style="color:#016F44">';
        echo '<li>[header cover="<b>url</b>"|covers="<b>url</b>,<b>url</b>" colored="<b>true</b>|<b>false</b>" smaller="<b>true</b>|<b>false</b>"]<b>contenu</b>[/header]</li>';
        echo '<li>[cols_row][col]<b>contenu</b>[/col][/cols_row] ajouter d’autres colonnes</li>';
        echo '<li>[cta href="<b>lien</b>" bigger="<b>true</b>|<b>false</b>" negative="<b>true</b>|<b>false</b>"]<b>label</b>[/cta]</li>';
        echo '<li>[text_image_section squared="<b>true</b>|<b>false</b>" image="<b>url</b>"]<b>contenu</b>[/text_image_section]';
        echo '<li>[tiles_grid][tile_text]<b>contenu</b>[/tile_text][tile_image src="<b>url</b>"][/tiles_grid] ajouter d’autres tuiles texte ou image';
        echo '<li>[subpages_list]</li>';
        echo '<li>[subpages_buttons]</li>';
        echo '</ul>';
        echo '<p><u>Les composants préfabriqués</u><br>éditables via les menus Apparence / Personnaliser et Langue / Traductions</p>';
        echo '<ul style="color:#016F44">';
        echo '<li>[connection_tpl departure="<b>expression</b>" arrival="<b>expression</b>"]</li>';
        echo '<li>[ctas_section]</li>';
        echo '</ul>';
    }
}

if (!function_exists('initialisation_metaboxes')) {
    function initialisation_metaboxes()
    {
        add_meta_box('shortcodes_infos', 'Shortcodes du thème', 'shortcodes_infos_meta_box', 'page', 'side', 'high');
    }
}
add_action('add_meta_boxes', 'initialisation_metaboxes');
