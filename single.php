<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <?php $sm_cover = get_template_directory_uri() . '/assets/images/ctas-section-sm-bg.webp';
    $cover = get_template_directory_uri() . '/assets/images/ctas-section-bg.webp'; ?>

    <article class="content">
        <section class="ctas-section bg-dark-primary [&_a:not(.cta)]:text-white text-white not-last:mb-8 lg:not-last:not-last:mb-16 relative">
            <img class="w-full !h-full !max-w-none object-cover object-center absolute top-0 left-0" alt="" src="<?php echo $cover; ?>" srcset="<?php echo $cover; ?> 1400w,<?php echo $$sm_cover; ?> 700w" sizes="100vw" />
            <div class="relative centered-row centered-row--smaller leading-0 py-16 text-center [&_p:not(:first-child)]:mt-11 [&_.ctas]:mt-4">
                <?php the_title('<h1 class="big-title">', '</h1>'); ?>
                <p><?php the_date(); ?></p>
            </div>
        </section>
        <?php the_content(); ?>
    </article>

<?php endwhile;  ?>

<?php
get_footer();
