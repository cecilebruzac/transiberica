<?php get_header(); ?>
<article class="content">
    <?php $sm_cover = get_template_directory_uri() . '/assets/images/ctas-section-sm-bg.webp';
    $cover = get_template_directory_uri() . '/assets/images/ctas-section-bg.webp'; ?>
    <section class="ctas-section bg-dark-primary [&_a:not(.cta)]:text-white text-white not-last:mb-8 lg:not-last:not-last:mb-16 relative">
        <img class="w-full !h-full !max-w-none object-cover object-center absolute top-0 left-0" alt="" src="<?php echo $cover; ?>" srcset="<?php echo $cover; ?> 1400w,<?php echo $$sm_cover; ?> 700w" sizes="100vw" />
        <div class="relative centered-row centered-row--smaller leading-0 py-16 text-center [&_p:not(:first-child)]:mt-11 [&_.ctas]:mt-4">
            <h1 class="big-title"><?php echo __('Désolé, la page que vous recherchez n’existe pas… ou&nbsp;plus&nbsp;!', 'transiberica') ?></h1>
            <p><?php printf(__('Vous pouvez cliquer ici pour retourner sur la <a href="%s">page d’accueil</a> ou nous contacter pour signaler le&nbsp;problème.<br>Merci beaucoup.', 'transiberica'), get_home_url()) ?></p>
            <p><a class="cta cta--negative" href="<?php echo get_home_url(); ?>"><?php echo __('Retour en page d’accueil', 'transiberica') ?></a></p>
        </div>
    </section>
</article>
<?php
get_footer();
