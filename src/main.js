function init() {
	document.body.classList.remove("no-js");

	const threshold = matchMedia('(max-width: 1023px)');

	function restoreBodyState() {
		const scrollY = document.body.style.top;
		document.body.style.top = '';
		document.body.style.position = '';
		document.documentElement.setAttribute(
			'style',
			'scroll-behavior: unset'
		);
		window.scrollTo(0, parseInt(scrollY || '0') * -1);
	}

	const fadeImgs = document.querySelectorAll('img.opacity-0');
	if (fadeImgs.length) fadeImgs.forEach((img) => {
		if (img.complete) img.style.opacity = '1';
		else img.onload = function () { img.style.opacity = '1' };
	});


	function animateModalCheckbox(element, callback) {
		if (element) {
			let activated = false;
			element.addEventListener('change', (event) => {
				if (event.currentTarget.checked) {
					document.body.style.top = `-${window.scrollY}px`;
					document.body.style.position = 'fixed';
					activated = true;
					if (callback && typeof callback === 'function') callback(activated);
				} else {
					restoreBodyState();
					element.classList.add('fade-out');

					setTimeout(function () {
						element.classList.remove('fade-out');
						document.documentElement.removeAttribute('style');
					}, 200);
					activated = false;
					if (callback && typeof callback === 'function') callback(activated);
				}
			});
		}
	}

	const pllDropdowns = document.querySelectorAll('input[type=checkbox][id^=pll-dropdown]');
	if (pllDropdowns.length) pllDropdowns.forEach((dropdown) => {
		const labels = document.querySelectorAll('label[for=' + dropdown.id + ']');
		if (labels.length) dropdown.addEventListener('change', () => {
			labels.forEach((label) => label.setAttribute('aria-expanded', dropdown.checked));
		});
	});

	animateModalCheckbox(document.querySelector('#mobile-menu-shown'), function (activated) {
		const toggler = document.querySelector('#mobile-menu-shown');

		if (toggler) {
			const labels = document.querySelectorAll('label[for=' + toggler.id + ']');
			if (labels.length) labels.forEach((label) => label.setAttribute('aria-expanded', toggler.checked));

			threshold.addEventListener('change', function () {
				if (activated) {
					toggler.checked = false;
					if (labels.length) labels.forEach((l) => l.removeAttribute('aria-expanded'));
					restoreBodyState();
					activated = false;
				}
			});
		}
	});

	const mainNav = document.querySelector('#main-nav');
	const nextElement = mainNav && mainNav.nextElementSibling;
	const menu = document.querySelector('#menu');
	const menuWrapper = document.querySelector('#menu-modal');
	let mainNavInitialTop = mainNav && mainNav.getBoundingClientRect().top;
	let mainNavInitialHeight = mainNav && mainNav.offsetHeight;
	let menuHeight = menu && menu.offsetHeight;
	const limit = 200;

	let prevScrollPos = window.scrollY;

	if (menuWrapper) {
		const setWindowHeightVar = () => {
			menuWrapper.style.setProperty('--window-height', `${window.innerHeight}px`);
		}
		window.addEventListener('resize', setWindowHeightVar);
		setWindowHeightVar();
	}


	if (mainNav && nextElement && menu && menuWrapper) {
		let timeout;

		function resetInlineStyles() {
			mainNav.style.position = '';
			nextElement.style.marginTop = '';
			menu.style.marginTop = '';
			clearTimeout(timeout);
			menuWrapper.style.overflow = '';
		}

		function initMainNavScrollAnim() {
			const currentScrollPos = window.scrollY;

			if (currentScrollPos > mainNavInitialTop) {
				mainNav.style.position = 'fixed';
				nextElement.style.marginTop = mainNavInitialHeight + 'px';
				clearTimeout(timeout);

				if (currentScrollPos > limit && currentScrollPos > prevScrollPos) {
					menuWrapper.style.overflow = 'hidden';
					menu.style.marginTop = -1 * menuHeight + 'px';
				} else {
					menu.style.marginTop = '0px';
					timeout = setTimeout(function () {
						menuWrapper.style.overflow = '';
					}, 500);
				}
			} else {
				resetInlineStyles();
			}

			prevScrollPos = currentScrollPos;

			if (currentScrollPos <= 0) {
				mainNavInitialTop = mainNav.getBoundingClientRect().top;
			}
		}

		let isDesktop = !threshold.matches;
		if (isDesktop) {
			initMainNavScrollAnim();
			window.addEventListener('scroll', initMainNavScrollAnim);
		}


		threshold.addEventListener('change', function (event) {
			isDesktop = !event.matches;

			if (isDesktop) {
				mainNavInitialTop = mainNav.getBoundingClientRect().top;
				mainNavInitialHeight = mainNav.offsetHeight;
				menuHeight = menu.offsetHeight;
				initMainNavScrollAnim();
				window.addEventListener('scroll', initMainNavScrollAnim);

			} else {
				resetInlineStyles();
				window.removeEventListener('scroll', initMainNavScrollAnim);
			}
		});
	}
}

window.onload = init;
