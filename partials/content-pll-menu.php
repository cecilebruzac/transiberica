<?php
$pll_options_array = pll_the_languages(array('raw' => 1, 'hide_current' => 1));

$classes = '';
$id = 'pll-dropdown';

if (array_key_exists('classes', $args)) {
    $classes = $args['classes'];
}

if (array_key_exists('id_suffix', $args)) {
    $id .= '-' . $args['id_suffix'];
}

if (!!$pll_options_array) : ?>
    <div class="flex items-center relative z-10 <?php echo $classes; ?>">
        <input type="checkbox" class="peer/checkbox absolute left-[-99999px]" id="<?php echo $id; ?>" value="" name="<?php echo $id; ?>">
        <label aria-haspopup="listbox" aria-label="translate" class="peer-focus-visible/checkbox:ring cta cta--neutral border border-light-grey cursor-pointer peer-checked/checkbox:rounded-b-none peer-checked/checkbox:border-b-transparent" for="<?php echo $id; ?>">
            <img class="w-6 select-none" width="24" height="16" alt="<?php echo pll_current_language('name') ?>" alt="<?php echo pll_current_language('name') ?>" src="<?php echo get_template_directory_uri() . '/assets/images/' . pll_current_language('slug') . '.svg' ?>" />
            <svg class="ml-2" height="7" width="11" viewBox="0 0 11 7" role="img" aria-hidden="true">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/defs.svg#arrow-down"></use>
            </svg>
        </label>
        <ul role="listbox" tabindex="-1" aria-label="translation-options" class="peer-focus-visible/checkbox:ring invisible peer-checked/checkbox:visible absolute top-full w-full border-x border-b rounded-b-3xl border-light-grey bg-white">
            <?php foreach ($pll_options_array as $lang) : ?>
                <li role="option">
                    <a tabindex="0" class="pl-8 py-2 block" href="<?php echo $lang['url']; ?>">
                        <img class="w-6 select-none" width="24" height="16" alt="<?php echo $lang['name'] ?>" title="<?php echo $lang['name'] ?>" src="<?php echo get_template_directory_uri() . '/assets/images/' . $lang['slug'] . '.svg' ?>" />
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif;
