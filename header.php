<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <link rel="apple-touch-icon" sizes="180x180" href="https://www.transiberica.eu/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" media="(prefers-color-scheme: light)"
        href="https://www.transiberica.eu/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" media="(prefers-color-scheme: light)"
        href="https://www.transiberica.eu/favicon-16x16.png" />
    <link rel="icon" type="image/png" sizes="32x32" media="(prefers-color-scheme: dark)"
        href="https://www.transiberica.eu/favicon-light-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" media="(prefers-color-scheme: dark)"
        href="https://www.transiberica.eu/favicon-light-16x16.png" />
    <link rel="manifest" href="https://www.transiberica.eu/site.webmanifest" />
    <link rel="mask-icon" href="https://www.transiberica.eu/safari-pinned-tab.svg" color="#02AB69" />
    <meta name="msapplication-TileColor" content="#02AB69" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400..600&family=Newsreader:ital,wght@0,400..500;1,400..500&display=swap" rel="stylesheet">

    <meta name="google-site-verification" content="Bbqx_FcX4zsemY-xrEEnF2dHftLF99mcrDH7e6GqJpQ" />
</head>

<body <?php body_class('no-js'); ?>>
    <?php wp_body_open(); ?>

    <?php
    $banner_body = get_theme_mod('banner_body');
    if ($banner_body) : ?>
        <div class="bg-alt-black text-white [&>a]:text-primary [&>a]:font-semibold text-center small-text p-2"><?php echo $banner_body; ?></div>
    <?php endif; ?>

    <nav role="navigation" id="main-nav" aria-label="Main nav" class="w-full select-none z-10 flex lg:flex-wrap justify-between items-center sticky top-0 bg-white shadow-nav">
        <!-- TODO: scripts menu animation + transitions -->
        <input type="checkbox" class="lg:hidden peer menu-toggler absolute left-[-99999px]" name="mobile-menu-shown" id="mobile-menu-shown">

        <?php
        $custom_logo_id = get_theme_mod('custom_logo');
        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');

        if (has_custom_logo()) :
            $classes = 'ml-4 lg:ml-12 peer-checked:max-lg:hidden my-3';
            if (!is_front_page()) : ?><p class="<?php echo $classes ?>"><a class="block" href="<?php echo get_home_url(); ?>"><?php else : ?><h1 class="<?php echo $classes ?>"><?php endif; ?>
                        <img class="max-h-4 w-auto lg:max-h-5 max-w-none" height="<?php echo $logo[2]; ?>" width="<?php echo $logo[1]; ?>" src="<?php echo esc_url($logo[0]); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        <?php if (!is_front_page()) : ?></a></p><?php else : ?></h1><?php endif; ?>
        <?php endif; ?>

        <div class="max-lg:hidden flex space-x-5 flex-1 justify-end items-center mr-12 my-3">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'features',
                'container' => false,
                'container_id' => false,
                'menu_class' => 'flex items-center space-x-5',
                'menu_id' => false,
                'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu" >%3$s</ul>',
                'walker' => new Features_Menu_Walker()
            )); ?>

            <?php get_template_part('partials/content', 'pll-menu'); ?>

        </div>
        <label aria-controls="menu-modal" aria-label="Ouvrir ou fermer le menu" class="my-3 ml-auto menu-toggler__label mr-4 !px-4 lg:hidden peer-focus-visible:ring cta cta--neutral border border-light-grey peer-checked:max-lg:shadow-nav cursor-pointer z-10" for="mobile-menu-shown">
            <svg aria-hidden="true" focusable="false" class="fill-dark-primary" width="24" height="16" viewBox="0 0 24 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect y="7" width="24" height="2" rx="1" class="menu-toggler__bar" />
                <rect y="1" width="24" height="2" rx="1" class="menu-toggler__bar" />
                <rect y="13" width="24" height="2" rx="1" class="menu-toggler__bar" />
            </svg>
        </label>

        <?php if (has_nav_menu('main-nav')) : ?>

            <?php
            $classes = 'w-full lg:basis-full lg:border-t lg:border-light-grey ';
            $classes .= 'max-lg:flex max-lg:flex-col max-lg:overflow-auto max-lg:absolute max-lg:top-0 max-lg:h-screen max-lg:!h-[var(--window-height)] max-lg:px-4';
            $classes .= 'max-lg:bg-white max-lg:bg-[image:var(--menu-bg)] max-lg:bg-cover';
            ?>

            <div id="menu-modal" style="--menu-bg: url(<?php echo get_template_directory_uri(); ?>/assets/images/menu-bg.webp)" class="<?php echo $classes; ?>">

                <?php get_template_part('partials/content', 'pll-menu', array(
                    'classes' => 'lg:hidden mt-4 mb-8 [&_label]:shadow-nav mx-auto',
                    'id_suffix' => '2'
                )); ?>

                <?php wp_nav_menu(array(
                    'theme_location' => 'main-nav',
                    'container' => false,
                    'container_id' => false,
                    'menu_class' => 'lg:transition-all lg:ease-linear lg:duration-400 lg:flex lg:space-x-5 lg:flex-wrap lg:justify-center max-lg:my-auto lg:px-12 py-3',
                    'menu_id' => 'menu',
                    'depth' => 0,
                    'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu" >%3$s</ul>',
                    'walker' => new Main_Nav_Menu_Walker()
                )); ?>

                <?php wp_nav_menu(array(
                    'theme_location' => 'features',
                    'container' => false,
                    'container_id' => false,
                    'menu_class' => 'lg:hidden mt-8 space-y-8 text-center max-lg:pb-24 ',
                    'menu_id' => false,
                    'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu" >%3$s</ul>',
                    'walker' => new Features_Menu_Walker()
                ));
                ?>
            </div>
        <?php endif; ?>
    </nav>