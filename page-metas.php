<?php
$saved_pages = array();
$pages = get_pages(array('lang' => ''));
foreach ($pages as $page) {
    array_push($saved_pages, (object)[
        'id' => $page->ID,
        'permalink' => get_permalink($page->ID),
        'name' => $page->post_name,
        '_yoast_wpseo_title' => get_post_meta($page->ID, '_yoast_wpseo_title', true),
        '_yoast_wpseo_metadesc' => get_post_meta($page->ID, '_yoast_wpseo_metadesc', true),
        '_yoast_wpseo_content_score' => get_post_meta($page->ID, '_yoast_wpseo_content_score', true),
        '_yoast_wpseo_estimated_reading_time_minutes' => get_post_meta($page->ID, '_yoast_wpseo_estimated-reading-time-minutes', true)
    ]);
}

function findTextByValueInArray($fooArray, $searchValue)
{
    foreach ($fooArray as $bar) {
        if ($bar['value'] == $searchValue) {
            return $bar['text'];
        }
    }
}

$xml = simplexml_load_file(get_template_directory_uri() . '/transportroutierfranceespagneportugaltransiberica.WordPress.2024-04-22.xml');
?>
<h1>Loop on XML export from https://www.transiberica.eu/dev/</h1>
<ul>
    <?php foreach ($xml->channel->item as $item) {
    ?>

        <?php
        $content = $item->children('http://purl.org/rss/1.0/modules/content/');
        $wp = $item->children('http://wordpress.org/export/1.2/');

        $tests = [
            "transport-routier-france-", "transport-routier-espagne-", "transport-routier-portugal-", "transport-routier-belgique-", "transport-routier-pays-bas-", "transport-routier-allemagne-", "transport-routier-europe-du-nord-", "transport-routier-suede-finlande-norvege-danemark-", "transport-routier-royaume-uni-", "transport-routier-pologne-republique-tcheque-autriche-", "transport-routier-italie-",
            "transporte-terrestre-francia-", "transporte-terrestre-espana-", "transporte-terrestre-portugal-", "transporte-terrestre-belgica-", "transporte-terrestre-paises-bajos-", "transporte-terrestre-alemania-", "transporte-terrestre-suecia-finlandia-noruega-dinamarca-", "transporte-terrestre-reino-unido-", "transporte-terrestre-polonia-republica-checa-austria-", "transporte-terrestre-italia-",
            "road-transport-france-", "road-transport-spain-", "road-transport-portugal-", "road-transport-belgium-", "road-transport-netherlands-", "road-transport-germany-", "road-transport-sweden-finland-norway-denmark-", "road-transport-united-kingdom-", "road-transport-poland-czech-republic-austria-", "road-transport-italy-"
        ];

        $isLiaison = false;

        /*foreach ($tests as $test) {
            if (str_contains((string)$item->link, $test)) {
                $isLiaison = true;
            }
        }*/

        if (!$isLiaison) {

        ?>

            <li style="margin-top:20px">
                <?php

                echo '<b style="color:red">' . (preg_replace('/^' . preg_quote('https://cecilebruzac.fr/transiberica', '/') . '/', '', (string)$item->link)) . '</b><br />';

                foreach ($saved_pages as $page) {
                    if ((string)$item->link === $page->permalink) {
                        echo '<span style="color:blue"">';
                        echo '<b>in db _yoast_wpseo_title:</b> ' . $page->_yoast_wpseo_title;
                        echo '<br />';
                        echo '<b>in db _yoast_wpseo_metadesc:</b> ' . $page->_yoast_wpseo_metadesc;
                        echo '<br />';
                        echo '<b>in db _yoast_wpseo_content_score:</b> ' . $page->_yoast_wpseo_content_score;
                        echo '<br />';
                        echo '<b>in db _yoast_wpseo_estimated_reading_time_minutes:</b> ' . $page->_yoast_wpseo_estimated_reading_time_minutes;
                        echo '</span>';
                        echo '<br />';

                        /*foreach ($wp->postmeta as $meta) {
                            $key = (string)$meta->meta_key;
                             if (str_contains($key, '_yoast_wpseo_metadesc')) {
                                update_post_meta($page->id, '_yoast_wpseo_metadesc', (string)$meta->meta_value);
                            }

                            if (str_contains($key, '_yoast_wpseo_title')) {
                                update_post_meta($page->id, '_yoast_wpseo_title', (string)$meta->meta_value);
                            }

                            if (str_contains($key, '_yoast_wpseo_content_score')) {
                                update_post_meta($page->id, '_yoast_wpseo_content_score', (string)$meta->meta_value);
                            }

                            if (str_contains($key, '_yoast_wpseo_estimated-reading-time-minutes')) {
                                update_post_meta($page->id, '_yoast_wpseo_estimated-reading-time-minutes', (string)$meta->meta_value);
                            }

                            if (str_contains($key, '_yoast_wpseo_wordproof_timestamp')) {
                                update_post_meta($page->id, '_yoast_wpseo_wordproof_timestamp', (string)$meta->meta_value);
                            }
                        }*/
                    }
                }

                foreach ($wp->postmeta as $meta) {
                    $key = (string)$meta->meta_key;
                    if (str_contains($key, 'yoast')) {
                        echo '<b>from xml ' . $key . ':</b> ' . (string)$meta->meta_value;
                        echo '<br />';
                    }
                ?>
            </li>
<?php
                }
            }
        }
?>
</ul>